import requests
import calendar
from datetime import datetime
from collections import defaultdict


def format_date(dat):
    con_date = datetime.strptime(dat, '%d-%b-%Y')
    formatted_date = con_date.strftime('%Y, %m, %d')
    return formatted_date


start_date = input("Enter start date (YYYY-MM-DD): ")
start_dat = datetime.strptime(start_date, '%Y-%m-%d')
starting_date = start_dat.strftime('%Y, %m, %d')
end_date = input("Enter end date (YYYY-MM-DD): ")
end_dat = datetime.strptime(end_date, '%Y-%m-%d')
ending_date = end_dat.strftime('%Y, %m, %d')

url = 'http://adunits.datawrkz.com/production/interview/data.json'

response = requests.get(url)
if response.status_code == 200:
    data = response.json()
else:
    print("error", response.status_code)


def average(elements):
    return sum(elements) / len(elements)


def stock_prices(start, end):
    #  To get all the records where opening prices is greater than closing prices
    #  keeping a constraint : start_date < end_date

    filtered_data = [records for records in data if
                     (records['Open'] > records['Close']) & (start <= format_date(records['Date']) <= end)]
    for record in filtered_data:
        print(record['Date'], ':', record['Open'], ':', record['Close'])


def average_turn_over(start, end):
    #  To find average turn over between any 2 inputs dates
    #  keeping a constraint : start_date < end_date
    elements = [ele for ele in data if start <= format_date(ele['Date']) <= end]
    avg = 0
    for element in elements:
        avg += element['Turnover (Rs. Cr)']
    return avg


def average_change_diff(start, end):
    # To find Average change difference between high and low (high - low) between any 2 inputs dates
    # keeping a constraint : start_date < end_date

    change_diff = [diff for diff in data if start <= format_date(diff['Date']) <= end]
    avg_change = 0
    for diff in change_diff:
        change = diff['High'] - diff['Low']
        avg_change += change
    return avg_change


def month_wise_average():
    # Month wise average of the open and close price
    month_data = defaultdict(lambda: {'sum_open': 0, 'count_open': 0, 'sum_close': 0, 'count_close': 0})
    for mon in data:
        dates = datetime.strptime(mon['Date'], '%d-%b-%Y')
        month = dates.month
        month_data[month]['sum_open'] += mon['Open']
        month_data[month]['count_open'] += 1
        month_data[month]['sum_close'] += mon['Close']
        month_data[month]['count_close'] += 1
        month_avg = {}
        month_dict = {1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr',
                      5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug',
                      9: 'Sept', 10: 'Oct', 11: 'Nov', 12: 'Dec'}
        for month, count in month_data.items():
            avg_open = count['sum_open'] / count['count_open']
            avg_close = count['sum_close'] / count['count_open']
            month_avg[month_dict[month]] = {'open_avg': avg_open, 'close_avg': avg_close}

        return month_avg


def negative_volatility():
    # Find out complete negative volatility for day (difference between days low price
    # and high price where open price and high price are same)
    # and sort negative volatility by ascending and return only 10 records.

    sorted_volatility = sorted(
        [{'date': rec['Date'],
          'negative_volatility': (rec['High'] - rec['Low'])}
         for rec in data
         if rec['Open'] == rec["High"]],
        key=lambda x: x['negative_volatility'])

    ten_records = sorted_volatility[:10]
    for record in ten_records:
        print(f"Date: {record['date']}, Negative Volatility: {record['negative_volatility']}")


def name_wise_turnover():
    # Day name wise Turnover (Rs. Cr)'s average, minimum, maximum

    turnover_avg = {}
    turnover_min = {}
    turnover_max = {}

    for record in data:
        date_parts = format_date(record["Date"])
        date_new = date_parts.split(',')
        day_name = calendar.day_name[calendar.weekday(int(date_new[0]), int(date_new[1]), int(date_new[2]))]

        if day_name not in turnover_avg:
            turnover_avg[day_name] = []
            turnover_min[day_name] = float("inf")
            turnover_max[day_name] = float("-inf")

        turnover = record["Turnover (Rs. Cr)"]
        turnover_avg[day_name].append(turnover)
        turnover_min[day_name] = min(turnover_min[day_name], turnover)
        turnover_max[day_name] = max(turnover_max[day_name], turnover)

    # Calculating the average turnover for each day of the week
    for day_name in turnover_avg:
        avg = sum(turnover_avg[day_name]) / len(turnover_avg[day_name])
        turnover_avg[day_name] = avg

    for day_name in turnover_avg:
        print(f"Day: {day_name}")
        print(f"Average Turnover: {turnover_avg[day_name]:.2f} Rs. Cr")
        print(f"Minimum Turnover: {turnover_min[day_name]:.2f} Rs. Cr")
        print(f"Maximum Turnover: {turnover_max[day_name]:.2f} Rs. Cr")
        print()


stock_prices(starting_date, ending_date)
print(average_turn_over(starting_date, ending_date))
print(average_change_diff(starting_date, ending_date))
print(month_wise_average())
negative_volatility()
name_wise_turnover()
